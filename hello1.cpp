///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// file hello1.cpp
//
// Description: Program MUST have a using namespace std; line nearr the top
// <UST NOT qualify any Standard Library functions/methods/objects with std::
//
// @author Christopher Agcanas <agcanas8@hawaii.edu>
// @date  10 Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

using namespace std;

int main(){
   // doesnt use std::
   cout << "Hello World!\n";

   return 0;
}
